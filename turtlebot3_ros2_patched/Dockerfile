# Define what ROS2 distribution to use
ARG ROS_DISTRO=dashing

# Define the ROS2 parent image with the given distribution
FROM ros:${ROS_DISTRO}

# Install `turtlebot3-bringup` with its dependencies
# Install `libc6:armhf` for 32-bit executables
RUN sudo dpkg --add-architecture armhf \
    && apt-get update \
    && apt-get install -y \
        ros-${ROS_DISTRO}-turtlebot3-bringup \
        libc6:armhf \
    && rm -rf /var/lib/apt/lists/*

# Define the name of the current package
ENV PACKAGE_NAME=turtlebot3
# Create workdir for current package
WORKDIR /${PACKAGE_NAME}

# Copy everything to the container
COPY . ./

# Build a patched version for `LDS-01`
# Patch makes the `\scan` topic reliable so that `rosbag2` can record it
RUN git clone --single-branch --branch ros2-devel https://github.com/ROBOTIS-GIT/hls_lfcd_lds_driver.git \
    && cd hls_lfcd_lds_driver \
    && git apply ../hlds_laser_publisher_make_scan_reliable.diff \
    && . /opt/ros/$ROS_DISTRO/setup.sh \
    && colcon build --symlink-install \
    && cd ..

# Make sure the container has access to the connected hardware (`OpenCR1.0` and `LDS-01`)
ENV INITSYSTEM=on
ENV UDEV=1

# Define the model of `turtlebot3`
ENV TURTLEBOT3_MODEL=burger

# Define the port to which `OpenCR1.0` is connected
ENV OPENCR_PORT=/dev/ttyACM0

# Determine whether to update `OpenCR1.0`
ENV UPDATE_OPENCR="true"

# If desired, download and extract the `OpenCR1.0` firmware update for ROS2
RUN if [ "${UPDATE_OPENCR}" = "true" ]; then \
    apt-get update \
    && apt-get install -y \
        wget \
    && wget https://github.com/ROBOTIS-GIT/OpenCR-Binaries/raw/master/turtlebot3/ROS2/latest/opencr_update.tar.bz2 \
    && tar -xjf opencr_update.tar.bz2 \
    && rm -rf /var/lib/apt/lists/* opencr_update.tar.bz2 \
    ; fi

# Define ROS2 DDS domain
ENV ROS_DOMAIN_ID=232

# Determine whether to make the `ROS_DOMAIN_ID` based on `HOSTNAME`
ENV ROS_DOMAIN_ID_BASED_ON_HOSTNAME="false"

# Execute the script
CMD ["/bin/bash", "/turtlebot3/start.sh"]
