# Source ROS2 dependecies
. /server/install/local_setup.sh
# Source the current ROS2 package
. /${PACKAGE_NAME}/install/local_setup.sh

# Forward `SIGTERM` to subprocess
forward_sigterm() {
    echo "INFO: Caught \`SIGTERM\`, forwarding to subprocess"
    kill -INT "${subprocess_pid}"
}
# Set the trap for `SIGTERM`
trap 'forward_sigterm' SIGTERM

# Request `navigation2` from server
ROS_DOMAIN_ID=${SERVER_ROS_DOMAIN_ID} ros2 run ${PACKAGE_NAME} client &

# Wait for subprocess
subprocess_pid="${!}"
wait "${subprocess_pid}"
exit 0
