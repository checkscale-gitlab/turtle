# Copyright 2019 ROB768

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Source server for srv types dependency
. server/install/setup.sh

# Define the names of the services
SERVER_NODE_NAME="navigation2_slam_server"
SERVER_SRV_NAME_START=""${SERVER_NODE_NAME}"/start"
SERVER_SRV_TYPE_START=""${SERVER_NODE_NAME}"/srv/StartNavigation2"
SERVER_SRV_NAME_STOP=""${SERVER_NODE_NAME}"/stop"
SERVER_SRV_TYPE_STOP=""${SERVER_NODE_NAME}"/srv/StopNavigation2"

# Export with only digits
HOSTNAME_ROS_DOMAIN_ID=$(echo "${HOSTNAME}" | tr -cd '[[:digit:]]')
# Make sure the `ROS_DOMAIN_ID` is valid
if [[ "${HOSTNAME_ROS_DOMAIN_ID}" -lt "0" || "${HOSTNAME_ROS_DOMAIN_ID}" -gt "232" ]]; then
    echo "ERROR: \`ROS_DOMAIN_ID\` must be in interval [0, 232]"
    exit 1
fi

# Stop `navigation2_slam_server` once `SIGTERM` is trapped
sigterm_to_stop_service_call() {
    echo "INFO: Stopping `navigation2_slam_server` by calling service \`"${SERVER_SRV_NAME_STOP}"\`"

    # Stop `navigation2_slam_server`
    ros2 service call ${SERVER_SRV_NAME_STOP} ${SERVER_SRV_TYPE_STOP} "{ros_domain_id: ${HOSTNAME_ROS_DOMAIN_ID}}"
}
# Set the trap for `SIGTERM`
trap 'sigterm_to_stop_service_call' SIGTERM

# Start `navigation2_slam_server`
ros2 service call ${SERVER_SRV_NAME_START} ${SERVER_SRV_TYPE_START} "{ros_domain_id: ${HOSTNAME_ROS_DOMAIN_ID}, config: ${TURTLEBOT3_MODEL}, map: ${MAP}}"

# Wait forever
tail -f /dev/null & wait
exit 0
